api=2
core = 7.x
projects[pressflow][type] = "core"
projects[pressflow][download][type] = "get"
projects[pressflow][download][url] = "https://github.com/pressflow/7/archive/refs/tags/pressflow-7.95.zip"
projects[radar][type] = "profile"
projects[radar][download][type] = "git"
projects[radar][download][branch] = "7.x-2.x"
projects[radar][download][url] = "https://0xacab.org/radar/drupal-make.git"
