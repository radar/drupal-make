<?php

/**
 * @file
 * radar_event_ical.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function radar_event_ical_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'group_importer_list';
  $page->task = 'page';
  $page->admin_title = 'Group importer list';
  $page->admin_description = '';
  $page->path = 'node/%node/importers';
  $page->access = array(
    'logic' => 'and',
    'plugins' => array(
      0 => array(
        'name' => 'og_perm',
        'settings' => array(
          'perm' => 'administer group',
        ),
        'context' => array(
          0 => 'logged-in-user',
          1 => 'argument_entity_id:node_1',
        ),
        'not' => FALSE,
      ),
    ),
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Ical importers',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array(
    'node' => array(
      'id' => 1,
      'identifier' => 'Node: ID',
      'name' => 'entity_id:node',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_group_importer_list__panel';
  $handler->task = 'page';
  $handler->subtask = 'group_importer_list';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Group importers',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'group' => 'group',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'one';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'one_main' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Event ical importers for %node:title';
  $display->uuid = '191c0837-2f2e-4072-b758-a7d5b8474f24';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_group_importer_list__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-f7ee28ad-5c33-4837-95c2-e107aca2a2cd';
  $pane->panel = 'one_main';
  $pane->type = 'node_create_links';
  $pane->subtype = 'node_create_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'types' => 'event_ical_importer',
    'field_name' => 'og_group_ref',
    'context' => 'argument_entity_id:node_1',
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'create-event',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f7ee28ad-5c33-4837-95c2-e107aca2a2cd';
  $display->content['new-f7ee28ad-5c33-4837-95c2-e107aca2a2cd'] = $pane;
  $display->panels['one_main'][0] = 'new-f7ee28ad-5c33-4837-95c2-e107aca2a2cd';
  $pane = new stdClass();
  $pane->pid = 'new-9854a085-236a-4406-bf47-2833ab6ba76f';
  $pane->panel = 'one_main';
  $pane->type = 'views_panes';
  $pane->subtype = 'radar_event_importers-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '9854a085-236a-4406-bf47-2833ab6ba76f';
  $display->content['new-9854a085-236a-4406-bf47-2833ab6ba76f'] = $pane;
  $display->panels['one_main'][1] = 'new-9854a085-236a-4406-bf47-2833ab6ba76f';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-f7ee28ad-5c33-4837-95c2-e107aca2a2cd';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['group_importer_list'] = $page;

  return $pages;

}
