<?php

/**
 * @file
 * radar_services.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function radar_services_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'services create update delete operations'.
  $permissions['services create update delete operations'] = array(
    'name' => 'services create update delete operations',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'radar_services',
  );

  // Exported permission: 'services_search_api search from any index'.
  $permissions['services_search_api search from any index'] = array(
    'name' => 'services_search_api search from any index',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'non-authenticated user' => 'non-authenticated user',
    ),
    'module' => 'services_search_api',
  );

  return $permissions;
}
