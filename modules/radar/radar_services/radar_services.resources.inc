<?php

/**
 * Extends services_search_api.resources.inc
 */

function _radar_services_unpublished_resource_retrieve($group = NULL, $limit = 100) {
  $view = views_get_view('radar_unpublished');
  $view->set_display('master');
  $view->set_arguments(array($group));
  $view->set_items_per_page($limit);
  $view->pre_execute();
  $view->execute();
  $output = $view->result;
  return $output;
}

/**
 * Callback function for the index service call v1.1.
 *
 * Adds labels to referenced entities.
 *
 * @see _services_search_api_resource_retrieve()
 */
function _radar_services_search_api_resource_retrieve_1_2($index, $keys = '', $filter, $sort, $limit, $offset, $fields, $language) {
  $query = _radar_services_search_api_execute_query($index, $keys, $filter, $sort, $limit, $offset, $fields, $language);
  $result = $query->execute();
  $output = array(
    'result' => _radar_services_search_api_process_results($result, $query, $fields, $language, '1.2'),
    'count' => $result['result count'],
    'facets' => _radar_services_search_api_process_facets($result, $query, $fields, $language),
  );
  return $output;
}

/**
 * Callback function for the index service call v1.1.
 *
 * Adds facets and metadata.
 *
 * @see _services_search_api_resource_retrieve()
 */
function _radar_services_search_api_resource_retrieve_1_1($index, $keys = '', $filter, $sort, $limit, $offset, $fields, $language) {
  $query = _radar_services_search_api_execute_query($index, $keys, $filter, $sort, $limit, $offset, $fields, $language);
  $result = $query->execute();
  $output = array(
    'result' => _radar_services_search_api_process_results($result, $query, $fields, $language, '1.1'),
    'count' => $result['result count'],
    'facets' => _radar_services_search_api_process_facets($result, $query, $fields, $language),
  );
  return $output;
}

/**
 * Callback function for the index service call: v1.0.
 *
 * Adds language handling.
 *
 * @see _services_search_api_resource_retrieve()
 */
function _radar_services_search_api_resource_retrieve($index, $keys = '', $filter, $sort, $limit, $offset, $fields, $language) {
  $query = _radar_services_search_api_execute_query($index, $keys, $filter, $sort, $limit, $offset, $fields, $language);
  $result = $query->execute();
  return _radar_services_search_api_process_results($result, $query, $fields, $language, '1.0');
}

/**
 * Execute the search api query based on resource arguments.
 */
function _radar_services_search_api_execute_query($index, $keys, $filter, $sort, $limit, $offset, $fields, $language) {
  $options = array(
    'parse mode' => 'terms',
    'limit' => $limit,
    'offset' => $offset,
  );

  // Initialize the query object.
  $query = search_api_query($index, $options);
  if (!empty($keys)) {
    $query->keys($keys);
  }

  // Set the filters, transforming the query array in a SearchAPIQueryFilter
  // object.
  if (!empty($filter) && is_array($filter)) {
    module_load_include('inc', 'services_search_api', 'services_search_api.resources');
    $query_filter = new SearchApiQueryFilter();
    _services_search_api_add_filter($filter, $query_filter);
    $query->filter($query_filter);
  }

  // Also set sorts.
  if (!empty($sort) && is_array($sort)) {
    foreach ($sort as $field => $mode) {
      $query->sort($field, $mode);
    }
  }

  return $query;
}

/**
 * Adds language handling, also patched with @todo from makefile.
 *
 * @see _services_search_api_process_results()
 */
function _radar_services_search_api_process_results(&$result, $query, $fields, $language, $version) {
  if (empty($result['results']) || !is_array($result['results'])) {
    return FALSE;
  }

  module_load_include('inc', 'services_entity', 'services_entity.resources');

  $entity_type = $query->getIndex()->item_type;
  $entities = entity_load($entity_type, array_keys($result['results']));
  entity_make_entity_universal($entity_type, $entities);

  // Allow other modules to alter the entities retrieved.
  drupal_alter('services_search_api_postprocess', $entities, $entity_type);

  $resourceclass = variable_get('services_entity_resource_class', 'RadarServicesEntityResourceController');
  $resource = new $resourceclass();
  if (method_exists($resource, 'setApiVersion')) {
    $resource->setApiVersion($version);
  }
  $fields = implode(',', $fields);
  foreach ($entities as $entity_id => $entity) {
    // A bit confusing to do underneath. But swap language for taxonomy terms
    // translated with i18n_strings. Doing this in the formatting might be
    // better as it would catch referenced entities, but it's a loop from hell
    // and not priority at the moment.
    if ($entity_type == 'taxonomy_term') {
      $entity->name = i18n_string_translate('taxonomy:term:' . $entity->tid . ':name', $entity->name);
    }
    try {
      $entities[$entity_id] = $resource->format_entity($entity_type, $entity, $fields, $language);
    }
    catch (Exception $e) {
      watchdog_exception('services_search_api', $e);
      return;
    }
  }
  return $entities;
}

function _radar_services_search_api_process_facets(&$result, $query, $fields, $language) {
  $searcher = 'search_api@' . $query->getIndex()->machine_name;
  $adapter = facetapi_adapter_load($searcher);

  foreach ($result['search_api_facets'] as $name => $values) {
    $facet = facetapi_facet_load($name, $searcher);
    $facet_settings = $adapter->getFacetSettingsGlobal($facet);
    // Path alias defaults to facet_name.
    $field_alias = !empty($facet_settings->settings['pretty_paths_alias']) ? $facet_settings->settings['pretty_paths_alias'] : $facet['field alias'];

    $id = 'default';
    if (isset($facet) && isset($facet['facetapi pretty paths coder'])) {
      $id = $facet['facetapi pretty paths coder'];
    }
    if ($class = ctools_plugin_load_class('facetapi_pretty_paths', 'coders', $id, 'handler')) {
      $facet_api_pretty_paths_instance = new $class();
    }
    foreach ($values as $value) {
      $value['filter'] = check_plain(substr($value['filter'], 1, -1));
      switch ($name) {
        case 'og_group_ref':
          _radar_services_search_api_format_facets_group_title($value);
          break;
        case 'field_price_category':
        case 'field_topic':
        case 'field_category':
          _radar_services_search_api_format_facets_taxonomy_title($value);
          break;

        default:
          _radar_services_search_api_format_facets_plain($value);
      }

      if (!empty($facet_api_pretty_paths_instance)) {
        $segment = array(
          'alias' => $field_alias,
          'value' => $value['filter'],
        );
        $args = array(
          'segment' => &$segment,
          'facet' => $facet,
          'adapter' => $adapter,
        );
        call_user_func_array(array($facet_api_pretty_paths_instance, 'encodePathSegment'), array($args));
        $value['filter'] = $segment['value'];
      }

      $facets[$field_alias][] = $value;
    }
  }
  return $facets;
}

function _radar_services_search_api_format_facets_plain(&$value) {
  $value['formatted'] = $value['filter'];
}

function _radar_services_search_api_format_facets_taxonomy_title(&$value) {
  $id = $value['filter'];
  $term = taxonomy_term_load($id);
  $value['formatted'] = check_plain($term->name);
}
function _radar_services_search_api_format_facets_group_title(&$value) {
  $id = $value['filter'];
  $node = node_load($id);
  $value['formatted'] = check_plain($node->title);
}
