<?php
/**
 * @file
 * radar_languages.variable.inc
 */

/**
 * Implements hook_variable_realm_default_variables().
 */
function radar_languages_variable_realm_default_variables() {
$realm_variables = array();
  $realm_variables['language']['ar'] = array();
  $realm_variables['language']['ca'] = array();
  $realm_variables['language']['cs'] = array();
  $realm_variables['language']['de'] = array(
  'user_mail_password_reset_body' => '[user:name],

A request to reset the password for your account has been made at [site:name].

You may now log in by clicking this link or copying and pasting it to your browser:

[user:one-time-login-url]

This link can only be used once to log in and will lead you to a page where you can set your password. It expires after one day and nothing will happen if it\'s not used.

--  [site:name] team',
  'user_mail_password_reset_subject' => 'Replacement login information for [user:name] at [site:name]',
  'user_mail_register_admin_created_body' => '[user:name],

A site administrator at [site:name] has created an account for you. You may now log in by clicking this link or copying and pasting it to your browser:

[user:one-time-login-url]

This link can only be used once to log in and will lead you to a page where you can set your password.

After setting your password, you will be able to log in at [site:login-url] in the future using:

username: [user:name]
password: Your password

--  [site:name] team',
  'user_mail_register_admin_created_subject' => 'An administrator created an account for you at [site:name]',
  'user_mail_register_no_approval_required_body' => '[user:name],

Danke für deine/eure Registrierung auf [site:name]. 

Radar basiert auf Gruppen [Wie geht das: https://radar.squat.net/de/wie-geht-das]. Um Radar effektiv zu nutzen, solltet ihr darum entweder eine neue Gruppe einrichten oder Mitglied einer bestehenden Gruppe werden. 

Informationen darüber wie Gruppen eingerichtet werden, und wie neue Accounts Mitglieder von bestehenden Gruppent werden, findet ihr auf https://radar.squat.net/de/eine-gruppe-erstellen-und-benutzerinnen-zu-mitgliedern-machen. 
Falls ihr die Manager_innen einer bestehenden Gruppe nicht finden könnt, dann benutzt den Korrektur-Link auf der Gruppen-Seite.
Falls das alles nicht funktioniert, dann nehmt über radar-events@riseup.net Kontakt mit uns auf, und wir probieren, den Zugang für euch zu regeln. 

Ihr solltet jetzt schon eingelogt sein, aber ihr könnt auch EINMALIG einloggen, indem ihr diesen Link anklickt oder ihn in euren Browser kopiert. Danach müsst ihr euch dann an Euer Passwort erinnern ;-)

[user:one-time-login-url]

Ihr könnt in Zukunft einloggen mit: 

username: [user:name]
password: Your password

--  [site:name] Kollektiv',
  'user_mail_register_no_approval_required_subject' => 'Account details for [user:name] at [site:name]',
);
  $realm_variables['language']['el'] = array();
  $realm_variables['language']['en'] = array(
  'site_name' => 'radar.squat.net',
  'site_slogan' => 'Alternative and radical events agenda',
  'user_mail_password_reset_body' => '[user:name],

A request to reset the password for your account has been made at [site:name].

You may now log in by clicking this link or copying and pasting it to your browser:

[user:one-time-login-url]

This link can only be used once to log in and will lead you to a page where you can set your password. It expires after one day and nothing will happen if it\'s not used.

--  [site:name] team',
  'user_mail_password_reset_subject' => 'Replacement login information for [user:name] at [site:name]',
  'user_mail_register_admin_created_body' => '[user:name],

A site administrator at [site:name] has created an account for you. You may now log in by clicking this link or copying and pasting it to your browser:

[user:one-time-login-url]

This link can only be used once to log in and will lead you to a page where you can set your password.

After setting your password, you will be able to log in at [site:login-url] in the future using:

username: [user:name]
password: Your password

--  [site:name] collective',
  'user_mail_register_admin_created_subject' => 'An administrator created an account for you at [site:name]',
  'user_mail_register_no_approval_required_body' => '[user:name],

Thank you for your registration at [site:name]. 

Radar is based on groups [The Basics: https://radar.squat.net/en/how-to]. To use Radar effectivly, you should create a new group or become a member of an existing one.

Information about how to create groups and how to add accounts te existing groups can be found at: https://radar.squat.net/en/making-groups-and-adding-members. 
If you can\'t find the manager of an existing group, then try using the correction button on the page of the group. 
And if it all fails, then contact us at radar-events@riseup.net Kontakt and we try to verify and give you access.

You should already be logged in now, but you can log in ONCE ONLY by clicking this link or copy-pasting it into your browser. After that you will need to remember your password ;-)

[user:one-time-login-url]

In future you can log in with: 

username: [user:name]
password: Your password

--  [site:name] collective',
  'user_mail_register_no_approval_required_subject' => 'Account details for [user:name] at [site:name]',
);
  $realm_variables['language']['es'] = array();
  $realm_variables['language']['eu'] = array();
  $realm_variables['language']['fr'] = array(
  'user_mail_password_reset_body' => '[user:name],

A request to reset the password for your account has been made at [site:name].

You may now log in by clicking this link or copying and pasting it to your browser:

[user:one-time-login-url]

This link can only be used once to log in and will lead you to a page where you can set your password. It expires after one day and nothing will happen if it\'s not used.

--  [site:name] team',
  'user_mail_password_reset_subject' => 'Replacement login information for [user:name] at [site:name]',
  'user_mail_register_admin_created_body' => '[user:name],

UnE administratrice(teur) de [site:name] vient de vous créer un compte. Vous pouvez maintenant vous connectez en sélectionnant le lien ou en le copiant dans votre navigateur:

[user:one-time-login-url]

Ce lien n\'est utilisable qu\'une seule fois et vous mènera sur la page où vous entrez votre mot de passe.

Après avoir entré votre mot de passe, vous aurez la possibilité de vous connecter sur [site:login-url] en utilisant:

identifiant: [user:name]
mot de passe: Votre mot de passe

--  Collectif [site:name]',
  'user_mail_register_admin_created_subject' => 'À propos de votre compte sur [site:name]',
  'user_mail_register_no_approval_required_body' => '[user:name],

Thank you for your registration at [site:name]. 

Radar is based on groups [The Basics: https://radar.squat.net/en/how-to]. To use Radar effectivly, you should create a new group or become a member of an existing one.

Information about how to create groups and how to add accounts te existing groups can be found at: https://radar.squat.net/en/making-groups-and-adding-members. 
If you can\'t find the manager of an existing group, then try using the correction button on the page of the group. 
And if it all fails, then contact us at radar-events@riseup.net Kontakt and we try to verify and give you access.

You should already be logged in now, but you can log in ONCE ONLY by clicking this link or copy-pasting it into your browser. After that you will need to remember your password ;-)

[user:one-time-login-url]

In future you can log in with: 

username: [user:name]
password: Your password

--  [site:name] collective',
  'user_mail_register_no_approval_required_subject' => 'Account details for [user:name] at [site:name]',
);
  $realm_variables['language']['hu'] = array();
  $realm_variables['language']['it'] = array();
  $realm_variables['language']['ku'] = array();
  $realm_variables['language']['nl'] = array();
  $realm_variables['language']['nn'] = array();
  $realm_variables['language']['pl'] = array();
  $realm_variables['language']['pt-br'] = array();
  $realm_variables['language']['ru'] = array();
  $realm_variables['language']['sh'] = array();
  $realm_variables['language']['sl'] = array();
  $realm_variables['language']['sv'] = array();
  $realm_variables['language']['tr'] = array();

return $realm_variables;
}
