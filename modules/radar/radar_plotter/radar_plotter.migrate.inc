<?php

/**
 * Implements hook_migrate_api().
 */
function radar_plotter_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'plotter' => array(
        'title' => t('Plotter'),
      ),
    ),
    'migrations' => array(
      'PlotterTerminMigration' => array(
        'class_name' => 'PlotterTerminMigration',
        'group_name' => 'plotter',
      ),
      'PlotterGruppeMigration' => array(
        'class_name' => 'PlotterGruppeMigration',
        'group_name' => 'plotter',
      ),
      'PlotterOrtMigration' => array(
        'class_name' => 'PlotterOrtMigration',
        'group_name' => 'plotter',
      ),
      'PlotterOrtTermMigration' => array(
        'class_name' => 'PlotterOrtTermMigration',
        'group_name' => 'plotter',
      ),
      'PlotterKategorieMigration' => array(
        'class_name' => 'PlotterKategorieMigration',
        'group_name' => 'plotter',
      ),
    ),
  );
  return $api;
}


