<?php

/**
 * @file
 * radar_content_moderation.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function radar_content_moderation_user_default_roles() {
  $roles = array();

  // Exported role: regional administrator.
  $roles['regional administrator'] = array(
    'name' => 'regional administrator',
    'weight' => 5,
  );

  return $roles;
}
