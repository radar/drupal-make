<?php
/**
 * @file
 * radar_touring_group.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function radar_touring_group_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_url|node|touring_group|form';
  $field_group->group_name = 'group_url';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'touring_group';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Short url',
    'weight' => '11',
    'children' => array(
      0 => 'field_short_url',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Short url',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-url field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups[''] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Short url');

  return $field_groups;
}
