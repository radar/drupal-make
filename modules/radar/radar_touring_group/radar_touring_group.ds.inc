<?php
/**
 * @file
 * radar_touring_group.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function radar_touring_group_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|touring_group|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'touring_group';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h2',
        'class' => 'group touring-group',
        'ft' => array(),
      ),
    ),
    'node_link' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link text' => 'Read more & See all events >>',
        'link class' => '',
        'wrapper' => '',
        'class' => '',
        'ft' => array(),
      ),
    ),
    'field_group_logo' => array(
      'formatter_settings' => array(
        'ft' => array(
          'classes' => 'label',
        ),
      ),
    ),
  );
  $export['node|touring_group|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function radar_touring_group_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|touring_group|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'touring_group';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_group_logo',
        2 => 'body',
        3 => 'node_link',
        4 => 'field_category',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_group_logo' => 'ds_content',
      'body' => 'ds_content',
      'node_link' => 'ds_content',
      'field_category' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|touring_group|teaser'] = $ds_layout;

  return $export;
}
