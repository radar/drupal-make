<?php
/**
 * @file
 * radar_touring_group.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function radar_touring_group_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create touring_group content'.
  $permissions['create touring_group content'] = array(
    'name' => 'create touring_group content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'non-authenticated user' => 'non-authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any touring_group content'.
  $permissions['delete any touring_group content'] = array(
    'name' => 'delete any touring_group content',
    'roles' => array(
      'administrator' => 'administrator',
      'tech administrator' => 'tech administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own touring_group content'.
  $permissions['delete own touring_group content'] = array(
    'name' => 'delete own touring_group content',
    'roles' => array(
      'administrator' => 'administrator',
      'tech administrator' => 'tech administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any touring_group content'.
  $permissions['edit any touring_group content'] = array(
    'name' => 'edit any touring_group content',
    'roles' => array(
      'administrator' => 'administrator',
      'tech administrator' => 'tech administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own touring_group content'.
  $permissions['edit own touring_group content'] = array(
    'name' => 'edit own touring_group content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'non-authenticated user' => 'non-authenticated user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
