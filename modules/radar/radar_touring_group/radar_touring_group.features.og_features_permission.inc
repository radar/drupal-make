<?php

/**
 * @file
 * radar_touring_group.features.og_features_permission.inc
 */

/**
 * Implements hook_og_features_default_permissions().
 */
function radar_touring_group_og_features_default_permissions() {
  $permissions = array();

  // Exported og permission: 'node:touring_group:add user'
  $permissions['node:touring_group:add user'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:touring_group:administer group'
  $permissions['node:touring_group:administer group'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:touring_group:approve and deny subscription'
  $permissions['node:touring_group:approve and deny subscription'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:touring_group:create event content'
  $permissions['node:touring_group:create event content'] = array(
    'roles' => array(
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:touring_group:create event_ical_importer content'
  $permissions['node:touring_group:create event_ical_importer content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:touring_group:delete any event content'
  $permissions['node:touring_group:delete any event content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:touring_group:delete any event_ical_importer content'
  $permissions['node:touring_group:delete any event_ical_importer content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:touring_group:delete own event content'
  $permissions['node:touring_group:delete own event content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:touring_group:delete own event_ical_importer content'
  $permissions['node:touring_group:delete own event_ical_importer content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:touring_group:manage members'
  $permissions['node:touring_group:manage members'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:touring_group:manage permissions'
  $permissions['node:touring_group:manage permissions'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:touring_group:manage roles'
  $permissions['node:touring_group:manage roles'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:touring_group:subscribe'
  $permissions['node:touring_group:subscribe'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:touring_group:subscribe without approval'
  $permissions['node:touring_group:subscribe without approval'] = array(
    'roles' => array(
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:touring_group:unsubscribe'
  $permissions['node:touring_group:unsubscribe'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:touring_group:update any event content'
  $permissions['node:touring_group:update any event content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:touring_group:update any event_ical_importer content'
  $permissions['node:touring_group:update any event_ical_importer content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:touring_group:update group'
  $permissions['node:touring_group:update group'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:touring_group:update own event content'
  $permissions['node:touring_group:update own event content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:touring_group:update own event_ical_importer content'
  $permissions['node:touring_group:update own event_ical_importer content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  return $permissions;
}
