<?php

/**
 * @file
 * radar_user.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function radar_user_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'user_view__panel_context_b0127702-bad7-4e6f-9670-b8cf433a966c';
  $handler->task = 'user_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'one';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'one_main' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'd391ead4-8db7-4dd9-923b-a28ae31ec86c';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'user_view__panel_context_b0127702-bad7-4e6f-9670-b8cf433a966c';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-dcca3725-201a-476c-b0d2-c52b63b6f7ea';
  $pane->panel = 'one_main';
  $pane->type = 'views_panes';
  $pane->subtype = 'radar_user_groups-user_active_groups';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'argument_entity_id:user_1',
    ),
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'dcca3725-201a-476c-b0d2-c52b63b6f7ea';
  $display->content['new-dcca3725-201a-476c-b0d2-c52b63b6f7ea'] = $pane;
  $display->panels['one_main'][0] = 'new-dcca3725-201a-476c-b0d2-c52b63b6f7ea';
  $pane = new stdClass();
  $pane->pid = 'new-94656789-f6fe-41f8-ae37-7352f4373e60';
  $pane->panel = 'one_main';
  $pane->type = 'views_panes';
  $pane->subtype = 'radar_user_groups-user_proposed_groups';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'argument_entity_id:user_1',
    ),
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '94656789-f6fe-41f8-ae37-7352f4373e60';
  $display->content['new-94656789-f6fe-41f8-ae37-7352f4373e60'] = $pane;
  $display->panels['one_main'][1] = 'new-94656789-f6fe-41f8-ae37-7352f4373e60';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['user_view__panel_context_b0127702-bad7-4e6f-9670-b8cf433a966c'] = $handler;

  return $export;
}
