<?php

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Group membership contextual: Content create links'),
  'description' => t('Crafted links to create content (nodes) for a given group, based on the users access to post.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'category' => t('Organic groups'),
  'defaults' => array(
    'types' => array(),
    'field_name' => OG_AUDIENCE_FIELD,
  ),
);

/**
 * Render callback.
 */
function radar_og_node_create_links_content_type_render($subtype, $conf, $args, $context) {
  if (empty($context->data)) {
    return FALSE;
  }

  $node = $context->data;
  if ($conf['types'] == 'event') {
    $links = radar_event_og_node_create_links($node->nid);
  }
  elseif ($conf['types'] == 'event_ical_importer') {
    $links = radar_event_ical_og_node_create_links($node->nid);
  }
  else {
    throw new Exception(t('Create links does not currently support %type', ['%type' => $conf['types']]));
  }

  $links['radar_group_correction']['#markup'] = '<a class="correction" href="/correction?field_group=' . $node->nid . '">' . t('Send correction') . '</a>' ;
  if (!$links) {
    return FALSE;
  }

  $module = 'og';
  $block = new stdClass();
  $block->module = $module;
  $block->title = t('Content create links');

  $block->content = $links;
  return $block;
}

/**
 * Edit form.
 */
function radar_og_node_create_links_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $options = array();
  foreach (node_type_get_types() as $type) {
    if (og_is_group_content_type('node', $type->type)) {
      $options[$type->type] = check_plain($type->name);
    }
  }
  $form['types'] = array(
    '#title' => t('Restrict to content types'),
    '#type' => 'radios',
    '#options' => $options,
    '#default_value' => $conf['types'],
    '#description' => t('Presently supported event and ical.'),
    '#required' => TRUE,
  );
  return $form;
}

/**
 * Edit form submit callback.
 */
function radar_og_node_create_links_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['types'] = $form_state['values']['types'];
}
