(function ($) {
  Drupal.behaviors.radarLocationApiMap = {
    attach: function (context, settings) {
      /**
       * So this is probably not the way to do it but:
       *  (i) Learning experience,
       *  (ii) That reuses the styling settings from openlayers module.
       *
       * Really the layer should probably have a protocol that retrieves and
       * parses the data if I understand it correctly.
       */
      var map, layer, openlayers;

      map = $(context).data('openlayers').map;
      openlayers = $(context).data('openlayers').openlayers;
      for (layerIndex in $(context).data('openlayers').openlayers.layers) {
        if ($(context).data('openlayers').openlayers.layers[layerIndex].drupalID == 'radar_listed_groups_openlayers_1') {
          layer = $(context).data('openlayers').openlayers.layers[layerIndex];
          break;
        }
      }

			$.getJSON(Drupal.settings.radarLocation.url, function( data ) {
        var newFeatures = [];
        var featureProjection = new OpenLayers.Projection('EPSG:4326');
        var mapProjection = new OpenLayers.Projection(map.projection);
				$.each( data.result, function( key, featureData ) {
           feature = new OpenLayers.Feature.Vector();
           feature.geometry = new OpenLayers.Geometry.Point(featureData.offline[0].map.lon, featureData.offline[0].map.lat).transform(featureProjection, mapProjection);
           featureData.name = '<strong><a href="' + featureData.url + '">' + featureData.title + '</a></strong>';
           featureData.description = featureData.offline[0].address.thoroughfare;
           var categories = [];
           $.each( featureData.category, function( key, value ) {
             categories.push(value.name);
           });
           if (categories.length) {
             featureData.description += '<br/>' + categories.join(', ');
           }
           feature.attributes = featureData;
           newFeatures.push(feature);
				});
			  layer.addFeatures(newFeatures);
        openlayers.zoomToExtent(layer.getDataExtent());
			});

    }
  };
}(jQuery));

