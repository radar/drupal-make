<?php

/**
 * @file
 *   Groups search page map for city level.
 */
$plugin = array(
  // the title in the admin
  'title' => t('Map: City Groups'),
  'single' => TRUE,
  'category' => array(t('Radar'), -9),
  'render callback' => 'radar_location_groups_city_map_content_type_render'
);


/**
* Run-time rendering of the body of the block (content type)
*/
function radar_location_groups_city_map_content_type_render($subtype, $conf, $context = NULL) {
  global $language;
  $arguments = ['language=' . $language->language];

  $block = new stdClass();
  $block->content = '';
  $block->title = '';

  $city = FALSE;
  $segments = radar_services_current_facet_arguments();
  $search = search_api_current_search();
	list($search_api_query, $search_data) = reset($search);
	if (empty($search_api_query)) {
		return $block;
	}
/*  
  if (!$search_data['result count']) {
    return $block;
  }
  // looks like we might need to trigger something as can be 0
*/
  $keys = $search_api_query->getKeys();
  foreach (element_children($keys) as $key) {
    $arguments[] = 'keys[]=' . urlencode($keys[$key]);
  }
  foreach ($segments as $segment) {
    if ($segment['alias'] == 'city') {
      $city = TRUE;
    }
    $arguments[] = 'facets[' . $segment['alias'] . '][]=' . $segment['value'];
  }

  if (!$city) {
    return $block;
  }
  
  // Map style set in the UI.
  $map = openlayers_map_load('clone_of_radar_node_field_offline');
  $map->data['hide_empty_map'] = FALSE;
  $render = openlayers_render_map($map);
  $url = "/api/1.2/search/groups.json?" . implode('&', $arguments) . '&fields=title,url,category,offline:address:thoroughfare,offline:map:lon,offline:map:lat';
  // @todo don't show if there is no results.
  $block->content = $render;
  openlayers_include();
  drupal_add_js(['radarLocation' => ['url' => $url]], 'setting');
  drupal_add_js(drupal_get_path('module', 'radar_location') . '/js/api_map.js');
  return $block;
}
