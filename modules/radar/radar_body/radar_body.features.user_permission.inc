<?php
/**
 * @file
 * radar_body.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function radar_body_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_flyer'.
  $permissions['create field_flyer'] = array(
    'name' => 'create field_flyer',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_rejected'.
  $permissions['create field_rejected'] = array(
    'name' => 'create field_rejected',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_flyer'.
  $permissions['edit field_flyer'] = array(
    'name' => 'edit field_flyer',
    'roles' => array(
      'administrator' => 'administrator',
      'regional administrator' => 'regional administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_rejected'.
  $permissions['edit field_rejected'] = array(
    'name' => 'edit field_rejected',
    'roles' => array(
      'administrator' => 'administrator',
      'regional administrator' => 'regional administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_flyer'.
  $permissions['edit own field_flyer'] = array(
    'name' => 'edit own field_flyer',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_rejected'.
  $permissions['edit own field_rejected'] = array(
    'name' => 'edit own field_rejected',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_flyer'.
  $permissions['view field_flyer'] = array(
    'name' => 'view field_flyer',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'non-authenticated user' => 'non-authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_rejected'.
  $permissions['view field_rejected'] = array(
    'name' => 'view field_rejected',
    'roles' => array(
      'administrator' => 'administrator',
      'regional administrator' => 'regional administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_flyer'.
  $permissions['view own field_flyer'] = array(
    'name' => 'view own field_flyer',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_rejected'.
  $permissions['view own field_rejected'] = array(
    'name' => 'view own field_rejected',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  return $permissions;
}
