<?php

/**
 * @file
 * radar_basics.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function radar_basics_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__blog';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 2;
  $handler->conf = array(
    'title' => 'Blog',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(
      1 => array(
        'identifier' => 'Node from Node (on Node: Groups [og_group_ref])',
        'keyword' => 'node_2',
        'name' => 'entity_from_field:og_group_ref-node-node',
        'delta' => '0',
        'context' => 'argument_entity_id:node_1',
        'id' => 1,
      ),
    ),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'blog' => 'blog',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'metatag_panels' => array(
      'enabled' => 1,
      'metatags' => array(
        'title' => array(
          'value' => '[node:title-field], [node:og-group-ref:0] @ [node:field_date_time] | [site:name]',
        ),
        'description' => array(
          'value' => '[node:summary]',
        ),
        'rating' => array(
          'value' => 'general',
        ),
        'referrer' => array(
          'value' => 'no-referrer',
        ),
        'image_src' => array(
          'value' => '[node:field_image]',
        ),
        'shortlink' => array(
          'value' => '[node:field_short_url]',
        ),
        'geo.position' => array(
          'value' => '[node:field-offline:0:field-map:lat],[node:field-offline:0:field-map:lon]',
        ),
      ),
    ),
    'name' => 'blog',
  );
  $display = new panels_display();
  $display->layout = 'two_66_33';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'two_66_33_top' => NULL,
      'two_66_33_first' => NULL,
      'two_66_33_second' => NULL,
      'two_66_33_bottom' => NULL,
      'one_main' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'c94ad0ef-1e15-4ef3-b863-9e20baf33e50';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'node_view__blog';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-271ca3f4-2ee1-4983-a168-bfc32a28f008';
  $pane->panel = 'two_66_33_first';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'build_mode' => 'full',
    'identifier' => '',
    'link' => 0,
    'context' => 'argument_entity_id:node_1',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'translation-links',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '271ca3f4-2ee1-4983-a168-bfc32a28f008';
  $display->content['new-271ca3f4-2ee1-4983-a168-bfc32a28f008'] = $pane;
  $display->panels['two_66_33_first'][0] = 'new-271ca3f4-2ee1-4983-a168-bfc32a28f008';
  $pane = new stdClass();
  $pane->pid = 'new-25bcff7a-07cf-42b5-9404-bd1b80626104';
  $pane->panel = 'two_66_33_first';
  $pane->type = 'node_created';
  $pane->subtype = 'node_created';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'format' => 'date',
    'context' => 'argument_entity_id:node_1',
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '25bcff7a-07cf-42b5-9404-bd1b80626104';
  $display->content['new-25bcff7a-07cf-42b5-9404-bd1b80626104'] = $pane;
  $display->panels['two_66_33_first'][1] = 'new-25bcff7a-07cf-42b5-9404-bd1b80626104';
  $pane = new stdClass();
  $pane->pid = 'new-4d963800-b4db-4a22-8799-3629fc427252';
  $pane->panel = 'two_66_33_first';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'argument_entity_id:node_1',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '4d963800-b4db-4a22-8799-3629fc427252';
  $display->content['new-4d963800-b4db-4a22-8799-3629fc427252'] = $pane;
  $display->panels['two_66_33_first'][2] = 'new-4d963800-b4db-4a22-8799-3629fc427252';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view__blog'] = $handler;

  return $export;
}
