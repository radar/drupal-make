<?php

/**
 * @file
 * Contains RadarSearchApiAlterInterfaceTranslations.
 */

/**
 * Search API data alteration callback that adds any name interface translations.
 *
 * Intended for taxonomy terms.
 */
class RadarSearchApiAlterInterfaceTranslations extends SearchApiAbstractAlterCallback {

  public function alterItems(array &$items) {
    foreach ($items as &$item) {
      $translations = '';
      $languages = language_list('enabled');
      foreach ($languages[1] as $lang_code => $language) {
        $translation = i18n_string_translate('taxonomy:term:' . $item->tid . ':name', $item->name, ['langcode' => $lang_code]);
        if ($translation != $item->name) {
          $translations .= " $translation ";
        }
      }

      $item->radar_search_api_name_interface_translations = $translations;
    }
  }

  public function propertyInfo() {
    return array(
      'radar_search_api_name_interface_translations' => array(
        'label' => t('Translated name'),
        'description' => t('Term name translations.'),
        'type' => 'text',
      ),
    );
  }

}
