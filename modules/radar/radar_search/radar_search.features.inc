<?php

/**
 * @file
 * radar_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function radar_search_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function radar_search_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function radar_search_default_search_api_index() {
  $items = array();
  $items['default_node_index'] = entity_import('search_api_index', '{
    "name" : "Default node index",
    "machine_name" : "default_node_index",
    "description" : "An automatically created search index for indexing node data. Might be configured to specific needs.",
    "server" : null,
    "item_type" : "node",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "50",
      "data_alter_callbacks" : { "search_api_alter_node_access" : { "status" : 1, "weight" : "0", "settings" : [] } },
      "processors" : {
        "search_api_case_ignore" : { "status" : 1, "weight" : "0", "settings" : { "strings" : 0 } },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\nh2 = 3\\nh3 = 2\\nstrong = 2\\nb = 2\\nem = 1.5\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : { "spaces" : "[^\\\\p{L}\\\\p{N}]", "ignorable" : "[-]" }
        }
      },
      "fields" : {
        "author" : { "type" : "integer", "entity_type" : "user" },
        "body:value" : { "type" : "text" },
        "changed" : { "type" : "date" },
        "created" : { "type" : "date" },
        "field_category" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_offline:field_address:administrative_area" : { "type" : "list\\u003Ctext\\u003E" },
        "field_offline:field_address:country" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_address:locality" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_address:organisation_name" : { "type" : "list\\u003Ctext\\u003E" },
        "field_offline:field_address:postal_code" : { "type" : "list\\u003Ctext\\u003E" },
        "field_offline:field_address:premise" : { "type" : "list\\u003Ctext\\u003E" },
        "field_offline:field_address:thoroughfare" : { "type" : "list\\u003Ctext\\u003E" },
        "field_offline:field_map:geom" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_map:latlon" : {
          "type" : "list\\u003Cstring\\u003E",
          "real_type" : "list\\u003Clocation\\u003E"
        },
        "field_offline:title" : { "type" : "list\\u003Ctext\\u003E" },
        "nid" : { "type" : "integer" },
        "promote" : { "type" : "boolean" },
        "search_api_language" : { "type" : "string" },
        "sticky" : { "type" : "boolean" },
        "title" : { "type" : "text", "boost" : "5.0" },
        "title_field" : { "type" : "string" },
        "type" : { "type" : "string" },
        "uuid" : { "type" : "text" }
      }
    },
    "enabled" : "0",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  $items['events'] = entity_import('search_api_index', '{
    "name" : "Events",
    "machine_name" : "events",
    "description" : null,
    "server" : "solr_abernathy",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [ "event" ] },
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "changed" : { "type" : "date" },
        "field_callout" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_category" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_date_time:duration" : { "type" : "list\\u003Cduration\\u003E" },
        "field_date_time:value" : { "type" : "list\\u003Cdate\\u003E" },
        "field_date_time:value2" : { "type" : "list\\u003Cdate\\u003E" },
        "field_offline" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "location" },
        "field_offline:field_address:administrative_area" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_address:country" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_address:locality" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_address:name_line" : { "type" : "list\\u003Ctext\\u003E" },
        "field_offline:field_address:postal_code" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_address:premise" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_address:thoroughfare" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_map:geom" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_map:latlon" : {
          "type" : "list\\u003Cstring\\u003E",
          "real_type" : "list\\u003Clocation\\u003E"
        },
        "field_offline:id" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_offline:title" : { "type" : "list\\u003Ctext\\u003E" },
        "field_price" : { "type" : "string" },
        "field_price_category" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_topic" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "nid" : { "type" : "integer" },
        "og_group_ref" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "search_api_aggregation_1" : { "type" : "date" },
        "search_api_aggregation_3" : { "type" : "date" },
        "search_api_language" : { "type" : "string" },
        "search_api_viewed" : { "type" : "text" },
        "status" : { "type" : "boolean" },
        "title_field" : { "type" : "string" },
        "type" : { "type" : "string" },
        "uuid" : { "type" : "string" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : { "default" : "0", "bundles" : { "event" : "event" } }
        },
        "search_api_metatag_alter_callback" : { "status" : 0, "weight" : "0", "settings" : [] },
        "radar_search_api_alter_exclude_event_date" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "search_api_aggregation_1" : {
                "name" : "first_start_date",
                "type" : "min",
                "separator" : "\\n\\n",
                "fields" : [ "field_date_time:value" ],
                "description" : "A Minimum aggregation of the following fields: Date \\u0026amp; Time \\u00bb Start date."
              },
              "search_api_aggregation_3" : {
                "name" : "first_end_date",
                "type" : "min",
                "separator" : "\\n\\n",
                "fields" : [ "field_date_time:value2" ],
                "description" : "A Minimum aggregation of the following fields: Date \\u0026amp; Time \\u00bb End date."
              }
            }
          }
        },
        "search_api_alter_add_viewed_entity" : { "status" : 1, "weight" : "0", "settings" : { "mode" : "search_index" } },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_language_control" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "lang_field" : "", "languages" : [] }
        },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] },
        "radar_search_api_interface_translation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "field_category" : true,
              "og_group_ref" : true,
              "field_price" : true,
              "field_offline" : true,
              "field_topic" : true,
              "title_field" : true,
              "field_price_category" : true,
              "field_offline:title" : true,
              "field_offline:field_address:name_line" : true,
              "field_offline:field_address:administrative_area" : true,
              "field_offline:field_address:postal_code" : true,
              "field_offline:field_address:thoroughfare" : true,
              "field_offline:field_address:premise" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "search_api_viewed" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 1,
          "weight" : "15",
          "settings" : { "fields" : {
              "field_category" : true,
              "og_group_ref" : true,
              "field_price" : true,
              "field_topic" : true,
              "title_field" : true,
              "field_price_category" : true,
              "search_api_viewed" : true,
              "field_offline:title" : true,
              "field_offline:field_address:name_line" : true,
              "field_offline:field_address:administrative_area" : true,
              "field_offline:field_address:locality" : true,
              "field_offline:field_address:thoroughfare" : true,
              "field_offline:field_address:premise" : true
            }
          }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : {
            "fields" : { "search_api_viewed" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 1,
          "weight" : "30",
          "settings" : {
            "fields" : { "title_field" : true, "search_api_viewed" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_porter_stemmer" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "fields" : {
              "search_api_viewed" : true,
              "field_offline:title" : true,
              "field_offline:field_address:name_line" : true
            },
            "exceptions" : "texan=texa"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always",
            "highlight_partial" : 0
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "1",
    "rdf_mapping" : []
  }');
  $items['groups'] = entity_import('search_api_index', '{
    "name" : "Groups index",
    "machine_name" : "groups",
    "description" : null,
    "server" : "solr_abernathy_groups_",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [ "group", "listings_group" ] },
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "field_active" : { "type" : "boolean" },
        "field_category" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_offline" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "location" },
        "field_offline:field_address:administrative_area" : { "type" : "list\\u003Ctext\\u003E" },
        "field_offline:field_address:country" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_address:locality" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_address:name_line" : { "type" : "list\\u003Ctext\\u003E" },
        "field_offline:field_address:postal_code" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_address:premise" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_address:thoroughfare" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_map:geom" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:field_map:lat" : { "type" : "list\\u003Cdecimal\\u003E" },
        "field_offline:field_map:latlon" : {
          "type" : "list\\u003Cstring\\u003E",
          "real_type" : "list\\u003Clocation\\u003E"
        },
        "field_offline:field_map:lon" : { "type" : "list\\u003Cdecimal\\u003E" },
        "field_offline:field_squat" : { "type" : "list\\u003Cstring\\u003E" },
        "field_offline:id" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_offline:title" : { "type" : "list\\u003Ctext\\u003E" },
        "field_topic" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_type" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "nid" : { "type" : "integer" },
        "og_group_ref" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "radar_group_listed_by" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "radar_group_listed_by:nid" : { "type" : "list\\u003Cinteger\\u003E" },
        "search_api_language" : { "type" : "string" },
        "search_api_viewed" : { "type" : "text" },
        "status" : { "type" : "boolean" },
        "title" : { "type" : "string" },
        "type" : { "type" : "string" },
        "uuid" : { "type" : "string" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : {
            "default" : "0",
            "bundles" : { "group" : "group", "listings_group" : "listings_group" }
          }
        },
        "search_api_metatag_alter_callback" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : { "search_api_aggregation_1" : {
                "name" : "Group fulltext",
                "type" : "fulltext",
                "fields" : [
                  "title",
                  "field_offline:field_address:name_line",
                  "field_offline:field_address:locality",
                  "field_offline:field_address:thoroughfare"
                ],
                "description" : "A Fulltext aggregation of the following fields: Title, Locations \\u00bb Address \\u00bb Building name, Locations \\u00bb Address \\u00bb Locality (i.e. City), Locations \\u00bb Address \\u00bb Thoroughfare (i.e. Street address)."
              }
            }
          }
        },
        "search_api_alter_add_viewed_entity" : { "status" : 1, "weight" : "0", "settings" : { "mode" : "search_index" } },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_language_control" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "lang_field" : "", "languages" : [] }
        },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "field_category" : true,
              "og_group_ref" : true,
              "field_topic" : true,
              "search_api_viewed" : true,
              "field_offline:title" : true,
              "field_offline:field_address:name_line" : true,
              "field_offline:field_address:administrative_area" : true,
              "field_offline:field_address:locality" : true,
              "field_offline:field_address:postal_code" : true,
              "field_offline:field_address:thoroughfare" : true,
              "field_offline:field_address:premise" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "search_api_viewed" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 1,
          "weight" : "15",
          "settings" : { "fields" : {
              "title" : true,
              "field_category" : true,
              "og_group_ref" : true,
              "field_offline" : true,
              "field_topic" : true,
              "search_api_viewed" : true,
              "field_offline:field_address:name_line" : true,
              "field_offline:field_address:administrative_area" : true,
              "field_offline:field_address:locality" : true,
              "field_offline:field_address:postal_code" : true,
              "field_offline:field_address:thoroughfare" : true,
              "field_offline:field_address:premise" : true
            }
          }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : {
            "fields" : { "search_api_viewed" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 1,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true, "search_api_viewed" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_porter_stemmer" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "fields" : {
              "search_api_viewed" : true,
              "field_offline:title" : true,
              "field_offline:field_address:name_line" : true,
              "field_offline:field_address:administrative_area" : true
            },
            "exceptions" : "texan=texa"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always",
            "highlight_partial" : 0
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "1",
    "rdf_mapping" : []
  }');
  $items['location'] = entity_import('search_api_index', '{
    "name" : "location",
    "machine_name" : "location",
    "description" : null,
    "server" : "db",
    "item_type" : "location",
    "options" : {
      "datasource" : { "bundles" : [] },
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "field_address:administrative_area" : { "type" : "string" },
        "field_address:country" : { "type" : "string" },
        "field_address:dependent_locality" : { "type" : "string" },
        "field_address:locality" : { "type" : "string" },
        "field_address:sub_administrative_area" : { "type" : "string" },
        "field_directions" : { "type" : "text", "boost" : "0.5" },
        "search_api_language" : { "type" : "string" },
        "title" : { "type" : "text", "boost" : "21.0" },
        "uuid" : { "type" : "string" }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  $items['terms'] = entity_import('search_api_index', '{
    "name" : "Terms",
    "machine_name" : "terms",
    "description" : null,
    "server" : "db",
    "item_type" : "taxonomy_term",
    "options" : {
      "datasource" : { "bundles" : [] },
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "description" : { "type" : "text" },
        "name" : { "type" : "text", "boost" : "21.0" },
        "radar_search_api_name_interface_translations" : { "type" : "text", "boost" : "21.0" },
        "search_api_language" : { "type" : "string" },
        "uuid" : { "type" : "string" },
        "vocabulary:machine_name" : { "type" : "string" },
        "vocabulary:name" : { "type" : "string" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_metatag_alter_callback" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : {
              "search_api_aggregation_1" : {
                "name" : "Translations: name",
                "type" : "fulltext",
                "separator" : "\\n\\n",
                "fields" : [ "name" ],
                "description" : "A Fulltext aggregation of the following fields: Name."
              },
              "search_api_aggregation_2" : {
                "name" : "Translations: description",
                "type" : "fulltext",
                "separator" : "\\n\\n",
                "fields" : [ "description" ],
                "description" : "A Fulltext aggregation of the following fields: Description."
              }
            }
          }
        },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_language_control" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "lang_field" : "", "languages" : [] }
        },
        "radar_search_api_interface_translation" : { "status" : 1, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "name" : true,
              "description" : true,
              "radar_search_api_name_interface_translations" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "name" : true, "description" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 1,
          "weight" : "15",
          "settings" : { "fields" : {
              "name" : true,
              "description" : true,
              "radar_search_api_name_interface_translations" : true
            }
          }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : {
            "fields" : {
              "name" : true,
              "description" : true,
              "radar_search_api_name_interface_translations" : true
            },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "name" : true, "description" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_porter_stemmer" : {
          "status" : 1,
          "weight" : "35",
          "settings" : {
            "fields" : {
              "name" : true,
              "description" : true,
              "radar_search_api_name_interface_translations" : true
            },
            "exceptions" : "texan=texa"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "40",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always",
            "highlight_partial" : 0
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function radar_search_default_search_api_server() {
  $items = array();
  $items['db'] = entity_import('search_api_server', '{
    "name" : "db",
    "machine_name" : "db",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : {
      "database" : "default:default",
      "min_chars" : "2",
      "partial_matches" : 0,
      "indexes" : {
        "terms" : {
          "name" : {
            "table" : "search_api_db_terms_text",
            "type" : "text",
            "boost" : "21.0"
          },
          "description" : { "table" : "search_api_db_terms_text", "type" : "text", "boost" : "1.0" },
          "uuid" : {
            "table" : "search_api_db_terms",
            "column" : "uuid",
            "type" : "string",
            "boost" : "1.0"
          },
          "search_api_language" : {
            "table" : "search_api_db_terms",
            "column" : "search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "vocabulary:name" : {
            "table" : "search_api_db_terms",
            "column" : "vocabulary_name",
            "type" : "string",
            "boost" : "1.0"
          },
          "vocabulary:machine_name" : {
            "table" : "search_api_db_terms",
            "column" : "vocabulary_machine_name",
            "type" : "string",
            "boost" : "1.0"
          },
          "radar_search_api_name_interface_translations" : {
            "table" : "search_api_db_terms_text",
            "type" : "text",
            "boost" : "21.0"
          }
        },
        "location" : {
          "title" : {
            "table" : "search_api_db_location_text",
            "type" : "text",
            "boost" : "21.0"
          },
          "uuid" : {
            "table" : "search_api_db_location",
            "column" : "uuid",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_directions" : {
            "table" : "search_api_db_location_text",
            "type" : "text",
            "boost" : "0.5"
          },
          "search_api_language" : {
            "table" : "search_api_db_location",
            "column" : "search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_address:country" : {
            "table" : "search_api_db_location",
            "column" : "field_address_country",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_address:administrative_area" : {
            "table" : "search_api_db_location",
            "column" : "field_address_administrative_area",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_address:sub_administrative_area" : {
            "table" : "search_api_db_location",
            "column" : "field_address_sub_administrative_area",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_address:locality" : {
            "table" : "search_api_db_location",
            "column" : "field_address_locality",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_address:dependent_locality" : {
            "table" : "search_api_db_location",
            "column" : "field_address_dependent_locality",
            "type" : "string",
            "boost" : "1.0"
          }
        }
      }
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  $items['solr'] = entity_import('search_api_server', '{
    "name" : "solr",
    "machine_name" : "solr",
    "description" : "",
    "class" : "search_api_solr_service",
    "options" : {
      "clean_ids" : true,
      "site_hash" : true,
      "scheme" : "http",
      "host" : "localhost",
      "port" : "8983",
      "path" : "\\/solr\\/radar",
      "http_user" : "",
      "http_pass" : "",
      "excerpt" : 0,
      "retrieve_data" : 0,
      "highlight_data" : 0,
      "skip_schema_check" : 0,
      "solr_version" : "",
      "http_method" : "AUTO",
      "log_query" : 0,
      "log_response" : 0,
      "commits_disabled" : 0
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  $items['solr_abernathy'] = entity_import('search_api_server', '{
    "name" : "Solr: Abernathy (events)",
    "machine_name" : "solr_abernathy",
    "description" : "",
    "class" : "search_api_solr_service",
    "options" : {
      "clean_ids" : true,
      "site_hash" : true,
      "scheme" : "https",
      "host" : "abernathy.mayfirst.org",
      "port" : "8983",
      "path" : "\\/solr\\/radar_89utehs9320dcg",
      "http_user" : "radar",
      "http_pass" : "xcqck89843GRbanu",
      "excerpt" : 0,
      "retrieve_data" : 0,
      "highlight_data" : 0,
      "skip_schema_check" : 0,
      "solr_version" : "",
      "http_method" : "AUTO",
      "log_query" : 0,
      "log_response" : 0,
      "commits_disabled" : 0
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  $items['solr_abernathy_groups_'] = entity_import('search_api_server', '{
    "name" : "Solr: Abernathy (groups)",
    "machine_name" : "solr_abernathy_groups_",
    "description" : "",
    "class" : "search_api_solr_service",
    "options" : {
      "clean_ids" : true,
      "site_hash" : true,
      "scheme" : "https",
      "host" : "abernathy.mayfirst.org",
      "port" : "8983",
      "path" : "\\/solr\\/radar_i4687frqkso89cnmp",
      "http_user" : "radar",
      "http_pass" : "xcqck89843GRbanu",
      "excerpt" : 0,
      "retrieve_data" : 0,
      "highlight_data" : 0,
      "skip_schema_check" : 0,
      "solr_version" : "",
      "http_method" : "AUTO",
      "log_query" : 0,
      "log_response" : 0,
      "commits_disabled" : 0
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_sort().
 */
function radar_search_default_search_api_sort() {
  $items = array();
  $items['events__search_api_aggregation_1'] = entity_import('search_api_sort', '{
    "index_id" : "events",
    "field" : "search_api_aggregation_1",
    "name" : "first_start_date",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "events__search_api_aggregation_1",
    "default_sort" : "1",
    "default_sort_no_terms" : "1",
    "default_order" : "asc",
    "options" : { "field_name" : "first_start_date" },
    "rdf_mapping" : []
  }');
  $items['events__search_api_relevance'] = entity_import('search_api_sort', '{
    "index_id" : "events",
    "field" : "search_api_relevance",
    "name" : "Relevance",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "events__search_api_relevance",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "search_api_relevance" },
    "rdf_mapping" : []
  }');
  $items['events__title_field'] = entity_import('search_api_sort', '{
    "index_id" : "events",
    "field" : "title_field",
    "name" : "Event title",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "events__title_field",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "asc",
    "options" : { "field_name" : "Event title" },
    "rdf_mapping" : []
  }');
  $items['groups__search_api_relevance'] = entity_import('search_api_sort', '{
    "index_id" : "groups",
    "field" : "search_api_relevance",
    "name" : "Relevance",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "groups__search_api_relevance",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "search_api_relevance" },
    "rdf_mapping" : []
  }');
  $items['groups__title'] = entity_import('search_api_sort', '{
    "index_id" : "groups",
    "field" : "title",
    "name" : "Title",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "groups__title",
    "default_sort" : "1",
    "default_sort_no_terms" : "1",
    "default_order" : "asc",
    "options" : { "field_name" : "Title" },
    "rdf_mapping" : []
  }');
  $items['terms__search_api_relevance'] = entity_import('search_api_sort', '{
    "index_id" : "terms",
    "field" : "search_api_relevance",
    "name" : "Relevance",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "terms__search_api_relevance",
    "default_sort" : "1",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "search_api_relevance" },
    "rdf_mapping" : []
  }');
  return $items;
}
